#!/bin/bash


hoge=$(ls | grep storage )
echo "Checking permission..."
sleep 2

if [ -z "$hoge" ];then
echo "Requesting permission to access storage"
sleep 2
termux-setup-storage
fi

echo "Checking update..."
sleep 3
apt update
sleep 2
apt -y upgrade

hoge=$(apt list | grep python)

if [ -z "$hoge" ];then
"Installing python..."
sleep 3
apt -y install python
"Installing youtube-dl..."
sleep 2
pip install youtube-dl
else
echo "Updating youtube-dl..."
pip install --upgrade youtube-dl
fi
hoge=$(ls ~/storage/shared | grep youtube_dl)

echo "Installing ffmpeg"
sleep 2
apt -y install ffmpeg

if [ -z "$hoge" ];then
echo "Creating download folder..."
sleep 4
mkdir ~/storage/shared/youtube_dl
fi

hoge=$(ls ~/ | grep bin)

if [ -z "$hoge" ];then
echo "Creating bin folder at ~/"
sleep 3
mkdir ~/bin
fi


place="/data/data/com.termux/files/home/bin/termux-url-opener"

cp ./termux-url-opener ~/bin
echo "Please restart termux."